import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Environments from '../pages/Workspaces/Environments';
import Flows from '../pages/Workspaces/Flows';
import Functions from '../pages/Workspaces/Functions';
import Integrations from '../pages/Workspaces/Integrations';
import Releases from '../pages/Workspaces/Releases';
import Routes from '../pages/Workspaces/Routes';

export default function WorkspaceRoutes() {
  return (
    <>
      <Switch>
        <Route path="/workspace/:id/flows" component={Flows} />
        <Route path="/workspace/:id/environments" component={Environments} />
        <Route exact path="/workspace/:id/functions" component={Functions} />
        <Route exact path="/workspace/:id/routes" component={Routes} />
        <Route exact path="/workspace/:id/integrations" component={Integrations} />
        <Route
          path="/workspace/:id/releases"
          component={Releases}
        />
      </Switch>
    </>
  );
}

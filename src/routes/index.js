/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import ForgotPassword from '../pages/ForgotPassword';
import ResetPassword from '../pages/ForgotPassword/ResetPassword';
import Login from '../pages/Login';
import Signup from '../pages/Signup';
import Studio from '../pages/Studio';
import Teams from '../pages/Teams';
import InviteTeam from '../pages/Teams/InviteTeam';
import Workspace from '../pages/Workspaces';
import WorkspaceList from '../pages/Workspaces/WorkspaceList';
import { isAuthenticated } from '../services/auth';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (isAuthenticated()
      ? (<Component {...props} />)
      : (
        <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      ))}
  />
);

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={() => <Redirect to="/login" />} />
      <Route path="/login" component={Login} />
      <Route path="/signup" component={Signup} />
      <Route path="/forgot-my-password" component={ForgotPassword} />
      <Route path="/reset-my-password" component={ResetPassword} />
      <PrivateRoute
        path="/workspace/:id/flow/:flowId/studio"
        component={Studio}
      />
      <PrivateRoute path="/workspaces" component={WorkspaceList} />
      <PrivateRoute path="/workspace/:id" component={Workspace} />
      <PrivateRoute exact path="/teams" component={Teams} />
      <PrivateRoute path="/teams/:id/invite" component={InviteTeam} />
      <Route path="*" component={() => <Redirect to="/login" />} />
    </Switch>
  );
}

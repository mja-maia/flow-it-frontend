import axios from 'axios';

import { getToken } from './auth';

const api = axios.create({
  baseURL: 'https://studio-api.sptitan.flowyt.com/api/v1',
});

api.interceptors.request.use(async (configs) => {
  const config = configs;
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;

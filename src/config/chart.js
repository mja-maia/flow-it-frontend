export const CHART_INITAL_STATE = {
  offset: {
    x: 0,
    y: 0,
  },
  nodes: {
    start: {
      id: 'start',
      type: 'start',
      position: {
        x: 100,
        y: 300,
      },
      properties: {
        name: 'start',
      },
      ports: {
        output1: {
          id: 'output1',
          type: 'right',
        },
      },
    },
  },
  links: {},
  selected: {},
  hovered: {},
};

import React, {
  useState, useRef, useEffect,
} from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation, Link as RouterLink } from 'react-router-dom';
import { toast } from 'react-toastify';

import { CircularProgress, Link } from '@material-ui/core';
import { Form } from '@unform/web';
import { TextField } from 'unform-material-ui';
import * as Yup from 'yup';

import api from '../../services/api';
import { login } from '../../services/auth';
import { Creators as UserActions } from '../../store/ducks/user';
import {
  Container, Content, Title, FormGroup, LoginButton, SignUp,
} from './styles';

export default function Login() {
  const formRef = useRef(null);
  const history = useHistory();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  const useQuery = () => new URLSearchParams(useLocation().search);
  const query = useQuery();

  const handleSubmit = async (data) => {
    setLoading(true);
    try {
      const schema = Yup.object().shape({
        email: Yup.string().email('Type valid email')
          .required('Email is required'),
        password: Yup.string().required('Password is required'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      const result = await api.post('/token/', data);
      if (result.status === 200) {
        const { user } = result.data.user;
        login(result.data.access);
        history.push('/workspaces');
        dispatch(UserActions.setUserData(user));
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
      toast.error('Invalid credentials');
      setLoading(false);
    }
  };

  useEffect(() => {
    const verifyActivationToken = async () => {
      const activation_token = query.get('activation_token');
      if (activation_token) {
        const result = await api.get(`/accounts/verify/${activation_token}/`);
        if (result.status === 204) {
          toast.success('Your account has been activated');
        }
      }
    };
    verifyActivationToken();
  }, [query]);

  return (
    <Container>
      <Content>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <Title>Sign In</Title>
          <FormGroup>
            <TextField label="Email" required name="email" />
          </FormGroup>
          <FormGroup>
            <TextField
              label="Password"
              required
              name="password"
              type="password"
            />
            <Link component={RouterLink} to="/forgot-my-password">
              Forgot my password
            </Link>
          </FormGroup>
          <LoginButton type="submit">
            {loading ? <CircularProgress /> : 'Sign In'}
          </LoginButton>
          <SignUp>Not a member?
            <u
              role="button"
              tabIndex={0}
              onKeyPress={() => {}}
              onClick={() => history.push('/signup')}
            > Sign Up
            </u>
          </SignUp>
        </Form>
      </Content>
    </Container>
  );
}

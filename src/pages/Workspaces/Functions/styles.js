import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;
  padding: 30px;
`;

export const Header = styled.div`
   width: 100%;
   margin-bottom: 30px;

  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const CodeWrapper = styled.div``;

export const Footer = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: flex-end;
`;

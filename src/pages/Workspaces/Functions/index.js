import React, { useState, useEffect, useRef } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
  Button, Table, TableHead, TableRow, TableCell, TableBody,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Form } from '@unform/web';
import moment from 'moment/moment';
import { TextField } from 'unform-material-ui';
import { useDebouncedCallback } from 'use-debounce';
import * as Yup from 'yup';

import FormGroup from '../../../components/Form/FormGroup';
import Label from '../../../components/Form/Label';
import Title from '../../../components/Form/Title';
import api from '../../../services/api';
import {
  Container, Header, CodeWrapper, Footer,
} from './styles';

export default function Functions() {
  const { id } = useParams();
  const history = useHistory();
  const formRef = useRef(null);
  const [isEditing, setIsEditing] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [function_data, setFunctionData] = useState('');
  const [initialFormData, setInitialFormData] = useState({});
  const [rows, setRows] = useState([]);

  const onClickRow = (row) => {
    setInitialFormData(row);
    setFunctionData(row.function_data);
    setShowForm(true);
    setIsEditing(true);
  };

  const StyledTableCell = withStyles(() => ({
    head: {
      backgroundColor: '#d6d6d6',
      color: 'black',
    },
    body: {
      fontSize: 14,
      cursor: 'pointer',
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);

  const handleSubmit = async (data) => {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().required('Name is required'),
        description: Yup.string().required('Description is required'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      if (!isEditing) {
        const result = await api.post('/function-file/', {
          ...data,
          function_data,
          workspace: id,
        });

        if (result.status === 201) {
          setRows([
            ...rows,
            result.data,
          ]);
          setInitialFormData({
            ...data,
            function_data,
            id: result.data.id,
          });
          setIsEditing(true);
          toast.success('Function saved');
        }
      } else {
        const result = await api.put(`/function-file/${initialFormData.id}/`, {
          ...data,
          function_data,
          workspace: id,
        });

        if (result.status === 200) {
          history.push(`/workspace/${id}/functions/`);
          toast.success('Function updated');
        }
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  const [debouncedCallback] = useDebouncedCallback(
    () => {
      formRef.current.submitForm();
    },
    3000,
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await api.get(`/function-file/?workspace__id=${id}`);
        if (result.status === 200) {
          const { results } = result.data;
          setRows(results);
        }
      } catch (e) {
        toast.error('Something is wrong');
      }
    };
    fetchData();
  }, [id]);

  return (
    <Container>
      <Header>
        <Title>Functions</Title>
        {
          !showForm && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowForm(true);
              }}
            >
              Create Function
            </Button>
          )
        }
      </Header>
      {
        showForm && (
          <Form
            onSubmit={handleSubmit}
            ref={formRef}
            initialData={initialFormData}
          >
            <FormGroup>
              <TextField required name="name" label="Name" />
            </FormGroup>
            <FormGroup>
              <TextField required label="Description" name="description" />
            </FormGroup>
            <CodeWrapper>
              <Label>Function:</Label>
              <MonacoEditor
                language="python"
                theme="vs-dark"
                height="600"
                value={function_data}
                onChange={(val) => {
                  setFunctionData(val);
                  debouncedCallback();
                }}
              />
            </CodeWrapper>
            <Footer>
              <Button
                color="primary"
                variant="contained"
                type="submit"
              >
                {isEditing ? 'Edit' : 'Save'}
              </Button>
            </Footer>
          </Form>
        )
      }
      {
        !showForm && (
          <Table>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell>Name</StyledTableCell>
                <StyledTableCell>Description</StyledTableCell>
                <StyledTableCell>Last Update</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {
                rows.map((row) => (
                  <StyledTableRow key={row.id} onClick={() => onClickRow(row)}>
                    <StyledTableCell>{row.name}</StyledTableCell>
                    <StyledTableCell>{row.description}</StyledTableCell>
                    <StyledTableCell>
                      {moment(row.updated_at).fromNow()}
                    </StyledTableCell>
                  </StyledTableRow>
                ))
              }
            </TableBody>
          </Table>
        )
      }
    </Container>
  );
}

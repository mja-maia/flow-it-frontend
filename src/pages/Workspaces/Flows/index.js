import React, {
  useState, useEffect, useCallback, useRef,
} from 'react';
import { FaRegTrashAlt, FaPencilAlt } from 'react-icons/fa';
import { useParams, useHistory } from 'react-router-dom';

import { Button } from '@material-ui/core';
import { Form } from '@unform/web';
import moment from 'moment/moment';
import { TextField } from 'unform-material-ui';
import * as Yup from 'yup';

import FormGroup from '../../../components/Form/FormGroup';
import Modal from '../../../components/Modal';
import api from '../../../services/api';
import {
  Container, Header, FlowsList, FlowsListItem, Title, HeaderFlowListItem, Date,
} from './styles';

export default function Flows() {
  const formRef = useRef(null);
  const [flowData, setFlowData] = useState({});
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [flows, setFlows] = useState([]);
  const { id } = useParams();
  const history = useHistory();

  const handleClickOnModalButton = async (data) => {
    try {
      const schema = Yup.object().shape({
        name: Yup.string()
          .min('3', 'Name must be at least 3 characters')
          .required('Name is required'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      if (!editMode) {
        const result = await api.post('/flows/', {
          name: data.name,
          workspace: id,
          flow_layout: {},
        });
        const flowId = result.data.id;
        if (result.status === 201) {
          history.push(`/workspace/${id}/flow/${flowId}/studio`);
        }
      } else {
        const result = await api.patch(`/flows/${flowData.id}/`, {
          name: data.name,
        });
        if (result.status === 200) {
          history.push(`/workspace/${id}/flow/${flowData.id}/studio`);
        }
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};
        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  const redirectToStudio = (flowId) => {
    history.push(`/workspace/${id}/flow/${flowId}/studio`);
  };

  const fetchFlows = useCallback(async () => {
    const result = await api.get(`/flows/?workspace__id=${id}`);

    if (result.status === 200) {
      setFlows(result.data.results);
    }
  }, [id]);

  const handleDelete = async (flowId) => {
    const result = await api.delete(`/flows/${flowId}/`);
    if (result.status === 204) {
      fetchFlows();
    }
  };


  useEffect(() => {
    fetchFlows();
  }, [fetchFlows]);

  useEffect(() => {
    if (!modalIsOpen) {
      setEditMode(false);
    }
  }, [modalIsOpen]);

  return (
    <Container>
      <Modal
        width={600}
        height={200}
        setModalIsOpen={setModalIsOpen}
        isOpen={modalIsOpen}
        footerButton
        onClick={() => formRef.current.submitForm()}
        buttonText="Go to flow"
      >
        <Form
          ref={formRef}
          onSubmit={handleClickOnModalButton}
          initialData={flowData}
        >
          <FormGroup>
            <TextField label="Flow name" required name="name" />
          </FormGroup>
        </Form>
      </Modal>
      <Header>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setModalIsOpen(true)}
        >Create Flow
        </Button>
      </Header>
      <FlowsList>
        {
          flows.map((flow) => (
            <FlowsListItem
              key={flow.id}
            >
              <HeaderFlowListItem>
                <FaPencilAlt
                  onClick={() => {
                    setEditMode(true);
                    setModalIsOpen(true);
                    setFlowData(flow);
                  }}
                  size={12}
                  color="#bababa"
                />
                <FaRegTrashAlt
                  onClick={() => handleDelete(flow.id)}
                  size={12}
                  color="#bababa"
                  style={{ marginLeft: 12 }}
                />
              </HeaderFlowListItem>
              <Title
                onClick={() => redirectToStudio(flow.id)}
              >{flow.name}
              </Title>
              <Date>Updated {moment(flow.updated_at).fromNow()}</Date>
            </FlowsListItem>
          ))
        }
      </FlowsList>
    </Container>
  );
}

import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100%;

  display: flex;
  /* justify-content: center; */
  flex-direction: column;
  padding-top: 16px;
`;

export const Header = styled.div`
  padding: 16px;
  display: flex;
  justify-content: flex-end;
`;

export const FlowsList = styled.div`
  display: grid;
  padding: 16px;
  width: 100%;
  height: 100%;
  grid-template-columns: repeat(auto-fill, minmax(210px, 1fr));
  grid-template-rows: repeat(auto-fill, 90px);
  grid-column-gap: 16px;
  grid-row-gap: 20px;

`;

export const FlowsListItem = styled.div`
  width: 100%;
  background: white;
  border-radius: 5px;
  height: 100%;
  padding: 0 16px;

  display: flex;
  justify-content: center;
  flex-direction: column;

  svg{
    cursor: pointer;
  }
`;

export const HeaderFlowListItem = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const Title = styled.div`
  margin-bottom: 5px;
  cursor: pointer;

  &:hover{
    text-decoration: underline;
  }
`;

export const Date = styled.div`
  font-size: 10px;
  font-weight: 500;
  color: #b8b8b8;
`;

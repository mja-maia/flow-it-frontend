import React, { useState, useEffect, useRef } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
  Button, Table, TableHead, TableRow, TableCell, TableBody, MenuItem,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Form } from '@unform/web';
import moment from 'moment/moment';
import { TextField, Select } from 'unform-material-ui';
import * as Yup from 'yup';

import FormGroup from '../../../components/Form/FormGroup';
import Title from '../../../components/Form/Title';
import api from '../../../services/api';
import {
  Container, Header, Footer,
} from './styles';

export default function Functions() {
  const { id } = useParams();
  const [flows, setFlows] = useState([]);
  const history = useHistory();
  const formRef = useRef(null);
  const [isEditing, setIsEditing] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [initialFormData, setInitialFormData] = useState({
    method: 'GET',
  });
  const [rows, setRows] = useState([]);

  const onClickRow = (row) => {
    setShowForm(true);
    setIsEditing(true);
    setInitialFormData(row);
  };

  const StyledTableCell = withStyles(() => ({
    head: {
      backgroundColor: '#d6d6d6',
      color: 'black',
    },
    body: {
      fontSize: 14,
      cursor: 'pointer',
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  }))(TableRow);

  const handleSubmit = async (data) => {
    try {
      const schema = Yup.object().shape({
        path: Yup.string().required('Name is required')
          .matches(/^\//, {
            message: 'Path must be start with slash',
            excludeEmptyString: true,
          }),
        method: Yup.string().required('Method is required'),
        flow: Yup.string().required('Flow is required'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      if (!isEditing) {
        const result = await api.post('/routes/', {
          ...data,
          workspace: id,
        });

        if (result.status === 201) {
          setRows([
            ...rows,
            result.data,
          ]);
          toast.success('Route saved');
        }
      } else {
        const result = await api.put(`/routes/${initialFormData.id}/`, {
          ...data,
          workspace: id,
        });

        if (result.status === 200) {
          setShowForm(false);
          setRows(rows.map((row) => {
            if (row.id === result.data.id) {
              return result.data;
            }
            return row;
          }));
          toast.success('Route updated');
        }
      }
      setInitialFormData({});
      setShowForm(false);
      history.push(`/workspace/${id}/routes`);
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await api.get(`/routes/?workspace__id=${id}`);
        if (result.status === 200) {
          const { results } = result.data;
          setRows(results);
        }
      } catch (e) {
        toast.error('Something is wrong');
      }
    };
    fetchData();
  }, [id]);

  useEffect(() => {
    const fetchFlows = async () => {
      const result = await api.get(`/flows/?workspace__id=${id}`);
      if (result.status === 200) {
        const { results } = result.data;
        setFlows(results);
        if (!isEditing && results.length) {
          setInitialFormData((oldState) => ({
            ...oldState,
            flow: results[0].id,
          }));
        }
      }
    };
    fetchFlows();
  }, [id, showForm, isEditing]);

  return (
    <Container>
      <Header>
        <Title>Routes</Title>
        {
          !showForm && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setIsEditing(false);
                setShowForm(true);
              }}
            >
              Create Route
            </Button>
          )
        }
      </Header>
      {
        showForm && (
          <Form
            onSubmit={handleSubmit}
            ref={formRef}
            initialData={initialFormData}
          >
            <FormGroup>
              <TextField required name="path" label="Path" />
            </FormGroup>
            <FormGroup>
              <TextField name="description" label="Description" />
            </FormGroup>
            <FormGroup>
              <Select
                name="method"
                label="Methods"
                required
              >
                <MenuItem value="GET">GET</MenuItem>
                <MenuItem value="POST">POST</MenuItem>
                <MenuItem value="PUT">PUT</MenuItem>
                <MenuItem value="DELETE">DELETE</MenuItem>
                <MenuItem value="PATCH">PATCH</MenuItem>
                <MenuItem value="TRACE">TRACE</MenuItem>
                <MenuItem value="OPTIONS">OPTIONS</MenuItem>
                <MenuItem value="CONNECT">CONNECT</MenuItem>
              </Select>
            </FormGroup>
            <FormGroup>
              <Select required name="flow" label="Flow">
                <MenuItem>Select Flow</MenuItem>
                {
                  flows.length && (
                    flows.map((flow) => (
                      <MenuItem
                        key={flow.id}
                        value={flow.id}
                      >{flow.name}
                      </MenuItem>
                    ))
                  )
                }
              </Select>
            </FormGroup>
            <Footer>
              <Button
                color="primary"
                variant="contained"
                type="submit"
              >
                {isEditing ? 'Edit' : 'Save'}
              </Button>
            </Footer>
          </Form>
        )
      }
      {
        !showForm && (
          <Table>
            <TableHead>
              <StyledTableRow>
                <StyledTableCell>Path</StyledTableCell>
                <StyledTableCell>Description</StyledTableCell>
                <StyledTableCell>Last Update</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {
                rows.map((row) => (
                  <StyledTableRow key={row.id} onClick={() => onClickRow(row)}>
                    <StyledTableCell>{row.path}</StyledTableCell>
                    <StyledTableCell>{row.description}</StyledTableCell>
                    <StyledTableCell>
                      {moment(row.updated_at).fromNow()}
                    </StyledTableCell>
                  </StyledTableRow>
                ))
              }
            </TableBody>
          </Table>
        )
      }
    </Container>
  );
}

import React, { useState, useEffect, useRef } from 'react';
import { FaRegTrashAlt, FaPencilAlt } from 'react-icons/fa';
import { useHistory } from 'react-router-dom';

import {
  CircularProgress, FormGroup, Button, MenuItem,
} from '@material-ui/core';
import { Form } from '@unform/web';
import { TextField, Select } from 'unform-material-ui';
import * as Yup from 'yup';


import Title from '../../../components/Form/Title';
import api from '../../../services/api';
import {
  Container,
  Content,
  Header,
  List,
  ListItem,
  ColorIdentifier,
  ActionButtons,
  Info,
  Name,
  Loader,
} from './styles';

export default function WorkspaceList() {
  const history = useHistory();
  const formRef = useRef(null);
  const [workspaceData, setWorkspaceData] = useState({});
  const [workspaces, setWorkspaces] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [loading, setLoading] = useState(true);
  const [teams, setTeams] = useState([]);

  const fetchData = async () => {
    setLoading(true);
    const result = await api.get('/workspaces/');

    if (result.status === 200) {
      setLoading(false);
      setWorkspaces(result.data.results);
    }
  };

  const fetchTeams = async () => {
    const result = await api.get('/teams/');
    if (result.status === 200) {
      setTeams(result.data.results);
    }
  };

  useEffect(() => {
    fetchTeams();
    fetchData();
  }, []);

  const handleSubmit = async (data, { reset }) => {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().min('3', 'Name must be at least 3 characters')
          .required('Name is required'),
        team: Yup.string().required('Team is required'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      if (!isEditing) {
        const result = await api.post('/workspaces/', {
          ...data,
          workspace_color: `${Math.floor(Math.random() * 16777215)
            .toString(16)}`,
        });
        if (result.status === 201) {
          fetchData();
        }
      } else {
        const result = await api.patch(`/workspaces/${workspaceData.id}/`, {
          name: data.name,
          description: data.description,
          team: data.team,
        });
        if (result.status === 200) {
          fetchData();
        }
      }
      setShowForm(false);
      reset();
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  const handleClickListItem = (workspaceId) => {
    history.push(`/workspace/${workspaceId}/flows`);
  };

  const handleDelete = async (workspaceId) => {
    const result = await api.delete(`/workspaces/${workspaceId}/`);
    if (result.status === 204) {
      fetchData();
    }
  };

  const getTeamNameByWorkspace = (teamId) => {
    const result = teams.find((team) => team.id === teamId);
    if (result) {
      return `${result.name} Team`;
    }
    return '';
  };


  return (
    <Container>
      <Content>
        <Header>
          <Title>Workspace List</Title>
          <Button
            variant="contained"
            color="primary"
            onClick={() => setShowForm(!showForm)}
          >
            {showForm ? 'Back' : 'Create new'}
          </Button>
        </Header>

        <Loader>
          {
            loading && (
              <CircularProgress />
            )
          }
        </Loader>

        {
          !loading && (
            <>
              {
                !showForm ? (
                  <List>
                    {
                      workspaces.map((workspace) => (
                        <ListItem
                          key={workspace.id}
                        >
                          <ColorIdentifier color={workspace.workspace_color
                            ? `#${workspace.workspace_color}` : '#ED659A'}
                          />
                          <Info>
                            <Name
                              onClick={() => handleClickListItem(workspace.id)}
                            >{workspace.name} - {getTeamNameByWorkspace(workspace.team)}
                            </Name>
                            <ActionButtons>
                              <FaRegTrashAlt
                                color="#bababa"
                                onClick={() => handleDelete(workspace.id)}
                              />
                              <FaPencilAlt
                                color="#bababa"
                                style={{ marginLeft: 12 }}
                                onClick={() => {
                                  setWorkspaceData(workspace);
                                  setShowForm(true);
                                  setIsEditing(true);
                                }}
                              />
                            </ActionButtons>
                          </Info>
                        </ListItem>
                      ))
                    }
                  </List>
                ) : (
                  <Form
                    ref={formRef}
                    onSubmit={handleSubmit}
                    initialData={workspaceData}
                  >
                    <FormGroup>
                      <TextField
                        label="Name"
                        name="name"
                        required
                      />
                    </FormGroup>
                    <FormGroup>
                      <TextField
                        label="Description"
                        name="description"
                      />
                    </FormGroup>
                    <FormGroup style={{ marginTop: 16 }}>
                      <Select
                        color="primary"
                        required
                        label="Team"
                        name="team"
                      >
                        <MenuItem>Select a Team</MenuItem>
                        {
                            teams.map((team) => (
                              <MenuItem
                                key={team.id}
                                value={team.id}
                              >{team.name}
                              </MenuItem>
                            ))
                          }
                      </Select>
                    </FormGroup>
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      style={{ marginTop: 16 }}
                    >
                      Save
                    </Button>
                  </Form>
                )
              }
            </>
          )
        }
      </Content>
    </Container>
  );
}

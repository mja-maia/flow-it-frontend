import React from 'react';
import { MdArrowBack } from 'react-icons/md';
import { useParams, useHistory } from 'react-router-dom';


import AsideMenu from '../../components/AsideMenu';
import AsideItem from '../../components/AsideMenu/AsideItem';
import WorkspaceRoutes from '../../routes/workspace';
import { Container, Content } from './styles';

export default function Workspace() {
  const { id } = useParams();
  const history = useHistory();
  return (
    <Container>
      <AsideMenu>
        <AsideItem onClick={
            () => history.push('/workspaces')
          }
        >
          <MdArrowBack />
        </AsideItem>
        <AsideItem
          onClick={() => history.push(`/workspace/${id}/flows`)}
        >Flows
        </AsideItem>
        <AsideItem
          onClick={() => history.push(`/workspace/${id}/environments`)}
        >Environments
        </AsideItem>
        <AsideItem
          onClick={() => history.push(`/workspace/${id}/functions`)}
        >Functions
        </AsideItem>
        <AsideItem
          onClick={() => history.push(`/workspace/${id}/routes`)}
        >Routes
        </AsideItem>
        <AsideItem
          onClick={() => history.push(`/workspace/${id}/releases`)}
        >Releases
        </AsideItem>
        <AsideItem
          onClick={() => history.push(`/workspace/${id}/integrations`)}
        >Integrations
        </AsideItem>
      </AsideMenu>
      <Content>
        <WorkspaceRoutes />
      </Content>
    </Container>
  );
}

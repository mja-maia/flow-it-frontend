import styled from 'styled-components';

export const Container = styled.div`
  height: 95%;
  display: flex;
`;

export const Content = styled.div`
  width: 85%;
  height: 100%;
`;

import React, { useState, useEffect, useRef } from 'react';
import { MdPublish } from 'react-icons/md';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
  FormControlLabel,
  Checkbox,
  FormGroup,
  Button,
  Table,
  TableBody,
  TableHead,
} from '@material-ui/core';
import { Form } from '@unform/web';
import moment from 'moment/moment';
import { TextField } from 'unform-material-ui';
import * as Yup from 'yup';

import Title from '../../../components/Form/Title';
import Modal from '../../../components/Modal';
import api from '../../../services/api';
import {
  Container, Header, SubTitle, PublishedUrls, StyledTableRow, StyledTableCell,
} from './styles';

export default function Releases() {
  const { id } = useParams();
  const formRef = useRef(null);
  const [enviromentsModalIsOpen, setEnvironmentsModalIsOpen] = useState(false);
  const [rows, setRows] = useState([]);
  const [publishedUrls, setPublishedUrls] = useState([]);

  const [isCreating, setIsCreating] = useState(false);
  const [showStep1, setShowStep1] = useState(false);
  const [showStep2, setShowStep2] = useState(false);
  const [environmentsSelected, setEnvironmentsSelected] = useState([]);
  const [releaseData, setReleaseData] = useState(null);

  const fetchReleaseInfo = async (releaseId) => {
    try {
      const result = await api.get(`/releases/${releaseId}`);
      if (result.status === 200) {
        setReleaseData(result.data);
      }
    } catch (e) {
      toast.error('Something is wrong');
    }
  };

  const showModalEnvironments = (releaseId) => {
    setEnvironmentsSelected([]);
    setEnvironmentsModalIsOpen(true);
    if (releaseId) {
      fetchReleaseInfo(releaseId);
    }
  };

  const handleSubmit = async (data) => {
    try {
      if (isCreating) {
        const schema = Yup.object().shape({
          name: Yup.string()
            .min('3', 'Name should be at least 3 characters')
            .required('Name is required'),
          description: Yup.string().required('Description is required'),
        });
        await schema.validate(data, {
          abortEarly: false,
        });
        const result = await api.post(`/releases/workspaces/${id}/`, data);
        if (result.status === 200) {
          setRows([...rows, result.data]);
        }
        setEnvironmentsModalIsOpen(false);
      } else {
        const result = await api.post(`/releases/${releaseData.id}/publish/`, {
          environments: environmentsSelected,
        });
        if (result.status === 200) {
          setPublishedUrls(result.data.urls);
          setShowStep1(false);
          setShowStep2(true);
        }
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      } else {
        toast.error('Something is wrong');
      }
    }
  };

  const handleCheckboxChange = (releaseId) => (event) => {
    if (event.target.checked) {
      setEnvironmentsSelected([...environmentsSelected, releaseId]);
    } else {
      setEnvironmentsSelected(
        environmentsSelected.filter((item) => item !== releaseId),
      );
    }
  };

  const renderModalButtonText = () => {
    if (isCreating) {
      return 'Create';
    }
    return 'Publish';
  };

  const publishRelease = (rowId) => {
    setIsCreating(false);
    setShowStep2(false);
    setShowStep1(true);
    showModalEnvironments(rowId);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await api.get(`/releases/?workspace__id=${id}`);
        if (result.status === 200) {
          setRows(result.data.results);
        }
      } catch (e) {
        toast.error('Something is wrong');
      }
    };
    fetchData();
  }, [id]);

  return (
    <Container>
      <Modal
        width={500}
        height={350}
        setModalIsOpen={setEnvironmentsModalIsOpen}
        isOpen={enviromentsModalIsOpen}
        footerButton={!showStep2}
        onClick={() => formRef.current.submitForm()}
        disabled={isCreating ? false : !environmentsSelected.length}
        buttonText={renderModalButtonText()}
      >
        {showStep1 && (
          <>
            <Form ref={formRef} onSubmit={handleSubmit}>
              <Title>Publish</Title>
              <SubTitle>Choose enviroment to publish</SubTitle>
              <div style={{ marginTop: 40 }}>
                {releaseData
                  && releaseData.environments.map((item) => (
                    <FormGroup row key={item.id}>
                      <FormControlLabel
                        control={<Checkbox />}
                        label={item.name}
                        value={item.name}
                        onChange={handleCheckboxChange(item.id)}
                      />
                    </FormGroup>
                  ))}
              </div>
            </Form>
          </>
        )}
        {showStep2 && (
          <>
            <Title>Published with success</Title>
            <SubTitle>Available published links</SubTitle>
            {publishedUrls.map((url) => (
              <PublishedUrls>{url}</PublishedUrls>
            ))}
          </>
        )}
        {isCreating && (
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup style={{ marginBottom: 16 }}>
            <TextField
              label="Name"
              name="name"
              required
            />
          </FormGroup>
          <FormGroup>
            <TextField
              label="Description"
              name="description"
              required
            />
          </FormGroup>
        </Form>
        )}
      </Modal>
      <Header>
        <Title>Releases</Title>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            setIsCreating(true);
            setShowStep1(false);
            setShowStep2(false);
            showModalEnvironments(null);
          }}
        >
          Create Release
        </Button>
      </Header>
      <Table>
        <TableHead>
          <StyledTableRow>
            <StyledTableCell>Name</StyledTableCell>
            <StyledTableCell>Description</StyledTableCell>
            <StyledTableCell>Last Updated</StyledTableCell>
            <StyledTableCell />
          </StyledTableRow>
        </TableHead>
        <TableBody>
          {rows.sort((a, b) => a.updated_at < b.updated_at)
            .map((row) => (
              <StyledTableRow key={row.id} style={{ cursor: 'default' }}>
                <StyledTableCell>{row.name}</StyledTableCell>
                <StyledTableCell>{row.description}</StyledTableCell>
                <StyledTableCell>
                  {moment(row.updated_at).fromNow()}
                </StyledTableCell>
                <StyledTableCell>
                  {row.published && (
                  <div>Published</div>
                  )}
                  {
                  !row.published && (
                    <Button
                      onClick={() => publishRelease(row.id)}
                      variant="contained"
                      color="primary"
                      endIcon={<MdPublish />}
                    >
                      Publish
                    </Button>
                  )
                }
                </StyledTableCell>
              </StyledTableRow>
            ))}
        </TableBody>
      </Table>
    </Container>
  );
}

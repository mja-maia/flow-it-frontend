import { TableRow, TableCell } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;
  padding: 30px;
`;

export const Header = styled.div`
   width: 100%;
   margin-bottom: 30px;

  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const CheckBoxWrapper = styled.div`
  display: flex;
  align-items: center;

  input{
    margin-right: 20px;
  }

  div{
    color: rgb(115,115,115);
    text-transform: capitalize;
  }
`;

export const SubTitle = styled.div`
  color: rgb(115,115,115);
  font-size: 12px;
`;

export const PublishedUrls = styled.div`
  margin-top: 16px;
  color: rgb(115,115,115);
`;


export const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: '#d6d6d6',
    color: 'black',
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

import React, { useState, useEffect, useRef } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import { FormControlLabel } from '@material-ui/core';
import { Form } from '@unform/web';
import { TextField, Checkbox } from 'unform-material-ui';
import * as Yup from 'yup';

import FormGroup from '../../../components/Form/FormGroup';
import Label from '../../../components/Form/Label';
import Modal from '../../../components/Modal';
import api from '../../../services/api';
import {
  Container, Title, Subtitle, Grid, GridItem,
} from './styles';

export default function Environments() {
  const formRef = useRef(null);
  const [environmentData, setEnvironmentData] = useState({});
  const [environment_variables, setEnvironmentsVariables] = useState('');
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [environments, setEnvironments] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    const fetchEnvironments = async () => {
      try {
        const result = await api.get(`/environment/?workspace__id=${id}`);
        if (result.status === 200) {
          setEnvironments(result.data.results);
        }
      } catch (e) {
        toast.error('Something is wrong');
      }
    };
    fetchEnvironments();
  }, [id]);

  const resetAndClose = (reset) => {
    reset();
    setModalIsOpen(false);
  };

  const handleClickOnModalButton = async (data, { reset }) => {
    // verifico se possui o id no formdata
    // para saber se é para fazer post ou patch
    const { name, debug } = data;
    try {
      const schema = Yup.object().shape({
        name: Yup.string()
          .min('3', 'Name must be at least 3 characters')
          .required('Name is required'),
        debug: Yup.boolean().required('Debug is required'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      if (environmentData.id) {
        const result = await api.patch(`/environment/${environmentData.id}/`, {
          name,
          environment_variables,
          debug,
          workspace: id,
        });
        if (result.status === 200) {
          setEnvironments(environments.map((item) => {
            if (item.id === environmentData.id) {
              return {
                id: environmentData.id,
                environment_variables,
                ...data,
              };
            }
            return item;
          }));
          resetAndClose(reset);
        }
      } else {
        const result = await api.post('/environment/', {
          name,
          environment_variables,
          workspace: id,
        });
        if (result.status === 201) {
          setEnvironments([
            ...environments,
            result.data,
          ]);
          resetAndClose(reset);
        }
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  };

  const handleEnvironmentItemClick = (item) => {
    setEnvironmentsVariables(item.environment_variables);
    setEnvironmentData(item);
    setModalIsOpen(true);
  };

  return (
    <Container>
      <Modal
        width={600}
        height={500}
        setModalIsOpen={setModalIsOpen}
        isOpen={modalIsOpen}
        footerButton
        onClick={() => formRef.current.submitForm()}
        buttonText="Save"
      >
        <Form
          onSubmit={handleClickOnModalButton}
          ref={formRef}
          initialData={environmentData}
        >
          <FormGroup>
            <TextField
              label="Name"
              name="name"
            />
          </FormGroup>
          <FormGroup>
            <FormControlLabel
              label="Debug"
              control={(
                <Checkbox
                  required
                  name="debug"
                />
              )}
            />
          </FormGroup>
          <div>
            <Label>Variables</Label>
            <MonacoEditor
              language="json"
              theme="vs-dark"
              height="200"
              value={environment_variables}
              onChange={(val) => {
                setEnvironmentsVariables(val);
              }}
            />
          </div>
        </Form>
      </Modal>
      <Title>
        Environments
      </Title>
      <Subtitle>
        Environments can be used for different functions in a workflow
      </Subtitle>
      <Grid>
        {
          environments.map((item) => (
            <GridItem
              key={item.id}
              onClick={() => handleEnvironmentItemClick(item)}
            >
              <h1>{item.name}</h1>
            </GridItem>
          ))
        }
        <GridItem onClick={() => {
          setModalIsOpen(true);
          setEnvironmentData({});
          setEnvironmentsVariables('');
        }}
        >
          <h2>Environment</h2>
          <h2>+</h2>
        </GridItem>
      </Grid>
    </Container>
  );
}

import styled from 'styled-components';

export const Container = styled.div`
  height: 100%;
  padding: 30px;
  color: rgb(115,115,115);
`;

export const Title = styled.h1`
  font-size: 20px;
  font-weight: 300;
`;

export const Grid = styled.div`
  display: grid;
  height: 100%;
  width: 100%;
  grid-template-columns: repeat(auto-fill, minmax(240px, 1fr));
  grid-template-rows: repeat(auto-fill,minmax(200px, 1fr));
  grid-gap: 40px;
  margin-top: 30px;
`;

export const GridItemHeader = styled.div`
  position: absolute;
  right: 8px;
  top: 8px;
  color: #D2D1D1;
  cursor: pointer;
`;

export const GridItem = styled.div`
  color: rgb(7,0,45);
  position: relative;
  font-weight: 400;
  width: 100%;
  height: 100%;
  background: white;
  border-radius: 8px;
  border: 1px solid rgba(36,111,197, 0.2);

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  img{
    width: 30%;
  }

  p{
    margin-top: 10px;
    color: #9b9b9b;
  }
`;

export const Subtitle = styled.div`
  font-size: 14px;
  font-weight: 300;
`;

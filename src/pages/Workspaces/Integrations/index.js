import React, { useState, useEffect } from 'react';
import { IoMdSettings } from 'react-icons/io';
import { toast } from 'react-toastify';

import dataBaseIcon from '../../../assets/images/database.png';
import api from '../../../services/api';
import Databases from './Databases';
import {
  Container, Title, Subtitle, Grid, GridItem, GridItemHeader,
} from './styles';

export default function Integrations() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [integration, setIntegration] = useState('');
  const [integrationsList, setIntegrationsList] = useState([]);

  useEffect(() => {
    const fetchIntegrationsList = async () => {
      try {
        const result = await api.get('/integration-list/');
        if (result.status === 200) {
          setIntegrationsList(result.data.results);
        }
      } catch (e) {
        toast.error('Something wrong has happened');
      }
    };

    fetchIntegrationsList();
  }, []);


  return (
    <Container>
      {
        integration === 'databases' && (
          <Databases
            integrations={integrationsList}
            modalIsOpen={modalIsOpen}
            setModalIsOpen={setModalIsOpen}
          />
        )
      }
      <Title>
        Integrations
      </Title>
      <Subtitle>
        Integrations can be used to facilitate automation of many processes.
      </Subtitle>
      <Grid>
        <GridItem>
          <GridItemHeader onClick={() => {
            setIntegration('databases');
            setModalIsOpen(true);
          }}
          >
            <IoMdSettings />
          </GridItemHeader>
          <img src={dataBaseIcon} alt="MySql" />
          <p>Databases</p>
        </GridItem>

      </Grid>
    </Container>
  );
}

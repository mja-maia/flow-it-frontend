import React, { useState, useCallback, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';


import {
  Select, MenuItem, InputLabel, TextField,
} from '@material-ui/core';

import FormControl from '../../../../components/Form/FormControl';
import Modal from '../../../../components/Modal';
import api from '../../../../services/api';
import { Container } from './styles';

function Databases({
  modalIsOpen, setModalIsOpen, integrations,
}) {
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [databases] = useState([
    {
      name: 'Mysql',
      sgbd: 'mysql',
    },
    {
      name: 'Postgres',
      sgbd: 'postgresql',
    },
    {
      name: 'Oracle',
      sgbd: 'oracle',
    },
    {
      name: 'SQL Server',
      sgbd: 'sqlalchemy-tds',
    },
  ]);

  const [formData, setFormData] = useState({
    sgbd: 'mysql',
    name: '',
    host: '',
    port: '',
    password: '',
  });

  const [integrationListId, setIntegrationListId] = useState(null);

  const handleChange = useCallback((event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  }, [formData, setFormData]);

  const handleSubmit = useCallback(async () => {
    try {
      if (formData.id) {
        const result = await api.put(`/integration/${formData.id}/`, {
          integration_variables: formData,
          name: 'database',
          integration_list: integrationListId,
          workspace: id,
        });
        if (result.status === 200) {
          toast.success('Integration updated with success');
        }
      } else {
        const result = await api.post('/integration/', {
          integration_variables: formData,
          name: 'database',
          integration_list: integrationListId,
          workspace: id,
        });
        if (result.status === 201) {
          toast.success('Integration created with success');
        }
      }
      setModalIsOpen(false);
    } catch (e) {
      toast.error('something wrong has happened');
    }
  }, [formData, setModalIsOpen, id, integrationListId]);

  useEffect(() => {
    const loadFormData = async () => {
      try {
        const result = await api.get(`/integration/?workspace__id?=${id}`);
        if (result.status === 200) {
          if (result.data.results.length) {
            const data = JSON
              .parse(result.data.results[0].integration_variables);
            setFormData({
              ...data,
              id: result.data.results[0].id,
            });
          }
          const integrationTypeId = integrations
            .find((item) => item.integration_key === 'sql_database').id;
          setIntegrationListId(integrationTypeId);
          setIsLoading(false);
        }
      } catch (e) {
        toast.error(e);
      }
    };

    loadFormData();
  }, [integrations, id]);

  return (
    <Container>
      <Modal
        width={500}
        height={350}
        setModalIsOpen={setModalIsOpen}
        isOpen={modalIsOpen}
        footerButton
        onClick={handleSubmit}
        disabled={isLoading}
        buttonText="Save"
      >
        <FormControl>
          <InputLabel id="demo-simple-select-label">Database</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="sgbd"
            value={formData.sgbd}
            onChange={(event) => {
              setFormData({
                ...formData,
                sgbd: databases
                  .find((item) => item.sgbd === event.target.value).sgbd,
              });
            }}
          >
            {
            databases.map((item) => (
              <MenuItem key={item.sgbd} value={item.sgbd}>{item.name}</MenuItem>
            ))
          }
          </Select>
        </FormControl>
        <FormControl>
          <TextField
            label="Name"
            name="name"
            onChange={handleChange}
            value={formData.name}
          />
        </FormControl>
        <FormControl>
          <TextField
            label="Host"
            name="host"
            onChange={handleChange}
            value={formData.host}
          />
        </FormControl>
        <FormControl>
          <TextField
            label="Port"
            name="port"
            onChange={handleChange}
            value={formData.port}
          />
        </FormControl>
        <FormControl>
          <TextField
            label="Password"
            name="password"
            onChange={handleChange}
            value={formData.password}
          />
        </FormControl>
      </Modal>
    </Container>
  );
}

export default Databases;

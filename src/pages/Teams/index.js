import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Paper,
  IconButton,
  Drawer,
  TextField,
  FormControl,
} from '@material-ui/core';
import { Edit, Person } from '@material-ui/icons';

import api from '../../services/api';
import {
  Container, ButtonWrapper, DrawerContent, Content,
} from './styles';

export default function Teams() {
  const history = useHistory();
  const [teams, setTeams] = useState([]);
  const [showDrawer, setShowDrawer] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [formData, setFormData] = useState({
    name: '',
    description: '',
    organization: '',
  });

  const handleSubmit = async () => {
    if (formData.name !== '' && formData.description !== '') {
      if (isEditing) {
        try {
          const result = await api.patch(`/teams/${formData.id}/`, formData);
          if (result.status === 200) {
            const index = teams.findIndex((team) => team.id === formData.id);
            const newArray = teams;
            teams[index] = formData;
            setTeams(newArray);
          }
        } catch (e) {
          toast.error('Error when try update team');
        }
      } else {
        try {
          const result = await api.post('/teams/', formData);
          if (result.status === 200) {
            setTeams([
              ...teams,
              result.data,
            ]);
          }
        } catch (e) {
          toast.error('Error when try add team');
        }
      }
      setFormData({
        name: '',
        description: '',
        organization: '',
      });
      setShowDrawer(false);
      setIsEditing(false);
    }
  };

  const handleEdit = (team) => {
    setIsEditing(true);
    setFormData(team);
    setShowDrawer(true);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await api.get('/teams');
        if (result.status === 200) {
          setTeams(result.data.results);
        }
      } catch (e) {
        toast.error('Something is wrong');
      }
    };

    fetchData();
  }, []);

  return (
    <Container>
      <Drawer
        anchor="right"
        open={showDrawer}
        onClose={() => setShowDrawer(false)}
        style={{ width: 400 }}
      >
        <DrawerContent>
          <FormControl fullWidth>
            <TextField
              label="Name"
              value={formData.name}
              onChange={(e) => setFormData({
                ...formData,
                name: e.target.value,
              })}
            />
          </FormControl>
          <FormControl fullWidth style={{ marginTop: 8 }}>
            <TextField
              label="Description"
              value={formData.description}
              onChange={(e) => setFormData({
                ...formData,
                description: e.target.value,
              })}
            />
          </FormControl>
          <FormControl fullWidth style={{ marginTop: 8 }}>
            <TextField
              label="Organization"
              value={formData.organization}
              onChange={(e) => setFormData({
                ...formData,
                organization: e.target.value,
              })}
            />
          </FormControl>
          <ButtonWrapper>
            <Button
              onClick={() => handleSubmit()}
              variant="contained"
              color="primary"
            >
              {isEditing ? 'Edit' : 'Create'}
            </Button>
          </ButtonWrapper>
        </DrawerContent>
      </Drawer>
      <Content>
        <ButtonWrapper>
          <Button
            onClick={() => {
              setIsEditing(false);
              setShowDrawer(true);
              setFormData({
                description: '',
                name: '',
                organization: '',
              });
            }}
            variant="contained"
            color="primary"
          >Create Team
          </Button>
        </ButtonWrapper>
        <TableContainer style={{ width: 700, marginTop: 20 }} component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Description</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {teams.map((team) => (
                <TableRow key={team.name}>
                  <TableCell component="th" scope="row">
                    {team.name}
                  </TableCell>
                  <TableCell align="right">{team.description}</TableCell>
                  <TableCell align="right">
                    <IconButton onClick={() => handleEdit(team)}>
                      <Edit />
                    </IconButton>
                    <IconButton
                      onClick={() => history.push(`/teams/${team.id}/invite`)}
                    >
                      <Person />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Content>
    </Container>
  );
}

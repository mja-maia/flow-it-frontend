import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 700px;
`;

export const ButtonWrapper = styled.div`
  margin-top: 40px;
  width: 100%;

  display: flex;
  justify-content: flex-end;
`;

export const DrawerContent = styled.div`
  width: 400px;
  padding: 20px;
`;

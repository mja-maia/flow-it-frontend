import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Paper,
  IconButton,
  TextField,
  FormControl,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';

import Modal from '../../../components/Modal';
import api from '../../../services/api';
import { Container, ButtonWrapper, Content } from '../styles';
import { ModalContent } from './styles';

export default function InviteTeam() {
  const { id } = useParams();
  const [team, setTeam] = useState([]);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [email, setEmail] = useState('');

  const fetchData = useCallback(async () => {
    const result = await api.get(`/teams/${id}`);
    if (result.status === 200) {
      setTeam(result.data);
    }
  }, [id]);

  const handleModalClick = async () => {
    if (email.length) {
      const isAlreadyMember = team.members.find(
        (member) => member.email === email,
      );
      if (isAlreadyMember) {
        toast.error('This emails already member');
      } else {
        const result = await api.post(`/teams/${id}/invite/`, {
          emails: [email],
        });
        if (result.status === 200) {
          setEmail('');
          setTeam({
            ...team,
            all_members: [
              ...team.all_members,
              {
                email,
                status: 'PENDING',
              },
            ],
          });
          setModalIsOpen(false);
          toast.success(`Invite sent! The member will appear on
            the list as soon as the invitation is accepted`);
        }
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, [id, fetchData]);

  return (
    <Container>
      <Modal
        width={400}
        height={250}
        setModalIsOpen={setModalIsOpen}
        isOpen={modalIsOpen}
        footerButton
        onClick={() => handleModalClick()}
        buttonText="Invite"
      >
        <ModalContent>
          <h4>Invite a collaborator to your team</h4>
          <FormControl>
            <TextField
              label="Email"
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
            />
          </FormControl>
        </ModalContent>
      </Modal>
      <Content>
        <ButtonWrapper>
          <Button
            onClick={() => {
              setModalIsOpen(true);
            }}
            variant="contained"
            color="primary"
          >
            Invite Collaborator
          </Button>
        </ButtonWrapper>
        <TableContainer style={{ width: 700, marginTop: 20 }} component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Email</TableCell>
                <TableCell align="right">Status</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {team
                && team.all_members
                && team.all_members.map((member, index) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {member.email}
                    </TableCell>
                    <TableCell align="right">{member.status}</TableCell>
                    <TableCell align="right">
                      <IconButton disabled>
                        <Delete />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Content>
    </Container>
  );
}

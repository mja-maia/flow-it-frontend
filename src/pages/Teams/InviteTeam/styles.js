import styled from 'styled-components';

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  h4{
    font-size: 20px;
    color: rgba(0, 0, 0, 0.87);
    font-weight: 400;
    margin-bottom: 16px;
    text-align: center;
  }
`;

import styled from 'styled-components';

import backgroundImage from '../../assets/images/bg-01.jpg';

export const Container = styled.div`
  display: flex;
  height: 100%;
  background-image: url(${backgroundImage});
  color: #555555;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  width: 680px;
  background-color: white;
  border-radius: 6px;
  padding: 33px 110px;

  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const Title = styled.h1`
  line-height: 1.2;
  font-weight: 300;
  font-size: 39px;
  margin-bottom: 30px;
  width: 100%;
  text-align: center;
`;

export const FormGroup = styled.div`
  margin: 13px 0;
  display: flex;
  flex-direction: column;
`;

export const Label = styled.span`
  font-size: 16px;
  line-height: 1.5;
  font-weight: 600;
`;

export const Input = styled.input`
  background: #f7f7f7;
  color: #333333;
  font-size: 18px;
  line-height: 1.2;
  height: 60px;
  padding: 0 20px;
  border: 1px solid #e6e6e6;
  border-radius: 10px;
  outline: none;
`;

export const LoginButton = styled.button`
  background-color: #333333;
  color: white;
  height: 60px;
  padding: 0 20px;
  border-radius: 10px;
  font-weight: 600;
  font-size: 16px;
  line-height: 1.2;
  margin-top: 13px;
  outline: none;
`;

export const SignUp = styled.div`
  margin-top: 60px;
  color: #999999;
  font-size: 14px;
  line-height: 1.5;
  width: 100%;
  text-align: center;
  cursor: pointer;
`;

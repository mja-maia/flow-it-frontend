import React, { useState, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import { CircularProgress, Button } from '@material-ui/core';
import { Form } from '@unform/web';
import { TextField } from 'unform-material-ui';
import * as Yup from 'yup';


import api from '../../services/api';
import {
  Container, Content, Title, FormGroup,
} from './styles';


export default function Signup() {
  const formRef = useRef(null);
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  // const [shouldDisable, setShouldDisable] = useState(true);


  const handleSubmit = async (data) => {
    setLoading(true);
    try {
      const schema = Yup.object().shape({
        first_name: Yup.string()
          .min('3', 'First name must to be at least 3 characters')
          .required('First name is required'),
        last_name: Yup.string()
          .min('3', 'Last name must to be at least 3 characters')
          .required('Last name is required'),
        email: Yup.string().email('Type valid email')
          .required('Email is required'),
        password: Yup.string()
          .min('6', 'Password must to be at least 6 characters')
          .required('Email is required'),
        password_2: Yup.string()
          .oneOf([Yup.ref('password'), null], 'Password must match'),

      });
      await schema.validate(data, {
        abortEarly: false,
      });
      const result = await api.post('/accounts/register/', data);
      if (result.status === 200 || result.status === 201) {
        history.push('/login');
        toast.success('Account create with sucess! Please Signin!');
        setLoading(false);
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }

      if (err.response) {
        const {
          email, first_name, last_name, password, password_2,
        } = err.response.data;
        const requestErrors = [
          ...email || [],
          ...first_name || [],
          ...last_name || [],
          ...password || [],
          ...password_2 || [],
        ];
        requestErrors.forEach((error) => {
          toast.error(error);
        });
      }

      setLoading(false);
    }
  };

  return (
    <Container>
      <Content>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <Title>Sign up</Title>
          <FormGroup>
            <TextField label="First Name" required name="first_name" />
          </FormGroup>
          <FormGroup>
            <TextField label="Last Name" required name="last_name" />
          </FormGroup>
          <FormGroup>
            <TextField label="E-mail" required name="email" />
          </FormGroup>
          <FormGroup>
            <TextField
              label="Password"
              required
              name="password"
              type="password"
            />
          </FormGroup>
          <FormGroup>
            <TextField
              label="Password Confirmation"
              required
              name="password_2"
              type="password"
            />
          </FormGroup>
          <Button
            // disabled={shouldDisable}
            type="submit"
          >
            {loading ? <CircularProgress /> : 'Create Your Account'}
          </Button>
        </Form>
      </Content>
    </Container>
  );
}

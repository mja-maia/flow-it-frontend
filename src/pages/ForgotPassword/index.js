import React, {
  useState, useRef,
} from 'react';
import { toast } from 'react-toastify';

import { CircularProgress } from '@material-ui/core';
import { Form } from '@unform/web';
import { TextField } from 'unform-material-ui';
import * as Yup from 'yup';

import api from '../../services/api';
import {
  Container, Content, Title, FormGroup, ResetButton,
} from './styles';

export default function Login() {
  const formRef = useRef(null);
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (data) => {
    setLoading(true);
    try {
      const schema = Yup.object().shape({
        email: Yup.string().email('Please, type a valid email address.')
          .required('Email is required.'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      const result = await api.post('/accounts/password_reset/', data);
      if (result.status === 200) {
        formRef.current.clearField('email');
        toast.success(`A password reset link has been sent to ${data.email}`);
        setLoading(false);
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
      formRef.current.clearField('email');
      toast.error(`An error occurred while trying to reset
        the password for ${data.email}, please make sure your user exists.`);
      setLoading(false);
    }
  };

  return (
    <Container>
      <Content>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <Title>Forgot your Password?</Title>
          <FormGroup>
            <TextField label="Enter your email" required name="email" />
            <ResetButton type="submit">
              {loading ? <CircularProgress /> : 'Reset my Password'}
            </ResetButton>
          </FormGroup>
        </Form>
      </Content>
    </Container>
  );
}

import React, {
  useState, useRef,
} from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import { CircularProgress } from '@material-ui/core';
import { Form } from '@unform/web';
import { TextField } from 'unform-material-ui';
import * as Yup from 'yup';

import api from '../../../services/api';
import {
  Container, Content, Title, FormGroup, ResetButton,
} from './styles';

export default function Login() {
  const formRef = useRef(null);
  const history = useHistory();
  const [loading, setLoading] = useState(false);

  const useQuery = () => new URLSearchParams(useLocation().search);
  const query = useQuery();

  const clearForm = () => {
    formRef.current.clearField('password');
    formRef.current.clearField('passwordConfirmation');
  };

  const handleSubmit = async (data) => {
    setLoading(true);
    try {
      const schema = Yup.object().shape({
        password: Yup.string().required('Password is required'),
        passwordConfirmation: Yup.string()
          .oneOf([Yup.ref('password'), null], 'Passwords must match'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      const uidb64 = query.get('uidb64');
      const token = query.get('token');
      const result = await api.post(`/accounts/reset/${uidb64}/${token}/`, data);
      if (result.status === 200) {
        clearForm();
        toast.success('Password updated with success!');
        setLoading(false);
        history.push('/login');
      }
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
      clearForm();
      toast.error(`An error occurred while trying to reset
        the password, please make sure the password match.`);
      setLoading(false);
    }
  };

  return (
    <Container>
      <Content>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <Title>Password Reset</Title>
          <FormGroup>
            <TextField label="New password" type="password" required name="password" />
            <TextField label="Confirm password" type="password" required name="passwordConfirmation" />
            <ResetButton type="submit">
              {loading ? <CircularProgress /> : 'Reset my Password'}
            </ResetButton>
          </FormGroup>
        </Form>
      </Content>
    </Container>
  );
}

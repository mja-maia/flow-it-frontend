import styled from 'styled-components';

export const Container = styled.div`
  min-height: 100vh;

  display: flex;
`;

export const Content = styled.div`
  width: ${(props) => (!props.nodeType || props.nodeType === 'start' ? 85 : 70)}%;
`;

export const Modal = styled.div`
  height: 100vh;
  width: 30%;
  padding: 16px;
`;

export const MenuTitle = styled.div`
  color: #a4a5a6;
  margin-bottom: 20px;
  font-size: 30px;
  font-weight: 400;
  text-transform: capitalize;
`;

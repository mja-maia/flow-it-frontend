import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { FlowChart } from '@mrblenny/react-flow-chart';
import * as actions from '@mrblenny/react-flow-chart/src/container/actions';
import { mapValues } from 'lodash';
import { useDebouncedCallback } from 'use-debounce';

import Port from '../../components/Chart/Port';
import SavedMark from '../../components/Chart/SavedMark';
import Sidebar from '../../components/Chart/Sidebar';
import SideBarFlowVar
  from '../../components/Chart/Sidebar/SidebarItem/SideBarFlowVar';
import SideBarIf from '../../components/Chart/Sidebar/SidebarItem/SideBarIf';
import SideBarJump
  from '../../components/Chart/Sidebar/SidebarItem/SideBarJump';
import SideBarRequest
  from '../../components/Chart/Sidebar/SidebarItem/SideBarRequest';
import SideBarResponse
  from '../../components/Chart/Sidebar/SidebarItem/SideBarResponse';
import SideBarValidation
  from '../../components/Chart/Sidebar/SidebarItem/SideBarValidation';
import SideBarWorkspaceVar
  from '../../components/Chart/Sidebar/SidebarItem/SideBarWorkspaceVar';
import api from '../../services/api';
import { Creators as ChartActions } from '../../store/ducks/chart';
import { ChangeUnderScoreToSpace } from '../../utils/text';
import { validateLink } from './chartValidation';
// import NodeInnerCustom from '../../components/NodeInner';
import {
  Container, Content, Modal, MenuTitle,
} from './styles';

export default function Dashboard() {
  const dispatch = useDispatch();
  const [nodeType, setNodeType] = useState(null);
  const { flowId } = useParams();
  const [showSaveMark, setShowSaveMark] = useState(false);

  const {
    nodesData,
    chart,
    selectedNode,
  } = useSelector((state) => state.chart);

  const handleSaveMark = () => {
    setShowSaveMark(true);
    setTimeout(() => {
      setShowSaveMark(false);
    }, 3000);
  };

  const saveChart = async () => {
    const result = await api.patch(`/flows/${flowId}/`, {
      flow_layout: chart,
    });
    if (result.status === 200) {
      handleSaveMark();
    }
  };

  const [saveChartDebouncedCallback] = useDebouncedCallback(
    () => {
      saveChart();
    },
    1500,
  );

  const [saveFlowDataDebouncedCallback] = useDebouncedCallback(
    async () => {
      const result = await api.patch(`/flows/${flowId}/`, {
        flow_data: nodesData,
      });
      if (result.status === 200) {
        handleSaveMark();
      }
    },
    1500,
  );

  const handleSideBarChange = (event, val = null, name) => {
    if (!val && !name && event) {
      dispatch(ChartActions
        .setNodesData(selectedNode,
          event.target.name, event.target.value));
    } else {
      dispatch(ChartActions.setNodesData(selectedNode, name, val));
    }
    saveFlowDataDebouncedCallback();
  };

  const updateChart = (chartTransformer) => {
    const newChart = chartTransformer(chart);
    dispatch(ChartActions.setChartByActions({ ...chart, ...newChart }));
    saveChartDebouncedCallback();
  };

  const updateChartNode = (func) => (...args) => updateChart(func(...args));
  const mapedActionCallbacks = mapValues(actions, updateChartNode);
  const bypass = (event) => mapedActionCallbacks[event];

  const onDeleteKeyInterceptor = () => {
    if (chart.selected.id === 'start') {
      return;
    }
    dispatch(ChartActions.deleteNodeData(chart.selected.id));
    mapedActionCallbacks.onDeleteKey();
  };

  const stateActionCallbacks = {
    onDragNode: bypass('onDragNode'),
    onDragCanvas: bypass('onDragCanvas'),
    onLinkStart: bypass('onLinkStart'),
    onLinkMove: bypass('onLinkMove'),
    onLinkComplete: bypass('onLinkComplete'),
    onLinkCancel: bypass('onLinkCancel'),
    onLinkMouseEnter: bypass('onLinkMouseEnter'),
    onLinkMouseLeave: bypass('onLinkMouseLeave'),
    onLinkClick: bypass('onLinkClick'),
    onCanvasClick: bypass('onCanvasClick'),
    onDeleteKey: onDeleteKeyInterceptor,
    onNodeClick: bypass('onNodeClick'),
    onNodeSizeChange: bypass('onNodeSizeChange'),
    onPortPositionChange: bypass('onPortPositionChange'),
    onCanvasDrop: bypass('onCanvasDrop'),
  };

  useEffect(() => {
    setNodeType(null);
    if (chart.selected.id) {
      const { id } = chart.selected;
      const node = chart.nodes[id];
      if (node && node.properties.name) {
        setNodeType(node.properties.name);
      }
      dispatch(ChartActions.setSelectedNode(chart.selected.id));
    }
  }, [chart.selected, dispatch, chart.nodes]);

  useEffect(() => {
    const fetchChart = async () => {
      const result = await api.get(`/flows/${flowId}/`);
      if (result.status === 200
        && Object.entries(result.data.flow_layout).length) {
        dispatch(ChartActions.setChart(result.data.flow_layout));
        if (result.data.flow_data !== null) {
          dispatch(ChartActions.setFlowData(result.data.flow_data));
        }
      } else {
        dispatch(ChartActions.resetChart());
      }
    };
    fetchChart();
  }, [dispatch, flowId]);

  useEffect(() => {
    const listener = (e) => {
      if (e.keyCode === 83
        && (navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey)) {
        e.preventDefault();
        return false;
      }
      return e;
    };
    document.addEventListener('keydown', listener);
    return () => {
      document.removeEventListener('keydown', listener);
    };
  }, []);

  const renderSideBar = () => {
    if (nodeType === 'request') {
      return (
        <SideBarRequest
          handleSideBarChange={handleSideBarChange}
        />
      );
    }

    if (nodeType === 'flow_var') {
      return <SideBarFlowVar handleSideBarChange={handleSideBarChange} />;
    }

    if (nodeType === 'workspace_var') {
      return <SideBarWorkspaceVar handleSideBarChange={handleSideBarChange} />;
    }

    if (nodeType === 'response') {
      return <SideBarResponse handleSideBarChange={handleSideBarChange} />;
    }

    if (nodeType === 'if') {
      return <SideBarIf />;
    }

    if (nodeType === 'validation') {
      return <SideBarValidation handleSideBarChange={handleSideBarChange} />;
    }

    if (nodeType === 'jump') {
      return <SideBarJump handleSideBarChange={handleSideBarChange} />;
    }
    return null;
  };

  return (
    <Container>
      {nodeType && nodeType !== 'start' ? (
        <Modal nodeType={nodeType}>
          <MenuTitle>
            {nodeType ? ChangeUnderScoreToSpace(nodeType) : ''}
          </MenuTitle>
          {
            renderSideBar()
          }
        </Modal>
      ) : (
        <Sidebar />
      )}
      <Content nodeType={nodeType}>
        <SavedMark showSaveMark={showSaveMark} />
        <FlowChart
          Components={{
            Port,
          }}
          chart={chart}
          callbacks={stateActionCallbacks}
          config={{
            validateLink,
          }}
        />
      </Content>
    </Container>
  );
}

const validateMultipleOutput = (props) => {
  const { chart, fromNodeId, toNodeId } = props;

  const value = Object.keys(chart.links)
    .filter((linkIndex) => {
      const chartFromNodeId = chart.links[linkIndex].from.nodeId;
      const chartToNodeId = chart.links[linkIndex].to.nodeId;
      return chartFromNodeId === fromNodeId
      || toNodeId === chartToNodeId;
    });
  console.log('value', value);

  return value.length > 2;
};

// linkId, fromNodeId, fromPortId, toNodeIdt, toPortId, char
export const validateLink = (props) => {
  const fromOutput = props.fromPortId === 'output1'
  || props.fromPortId === 'output2';
  const toOutput = props.toPortId === 'output1' || props.toPortId === 'output2';
  const fromInput = props.fromPortId === 'input';
  const toInput = props.toPortId === 'input';
  const toStart = props.toNodeId === 'start';

  if (fromOutput && toOutput) return false;
  if (fromOutput && toStart) return false;
  if (fromInput && toInput) return false;
  if (validateMultipleOutput(props)) return false;
  return true;
};

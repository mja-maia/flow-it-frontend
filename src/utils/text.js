export const ChangeUnderScoreToSpace = (text) => {
  if (text) {
    return text.replace('_', ' ');
  }
  return '';
};

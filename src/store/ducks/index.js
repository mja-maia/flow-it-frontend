import { combineReducers } from 'redux';

import chart from './chart';
import user from './user';

export default combineReducers({
  chart,
  user,
});

import { CHART_INITAL_STATE } from '../../config/chart';

// Action Types
export const Types = {
  SET_CHART_BY_ACTIONS: 'chart/SET_CHART_BY_ACTIONS',
  SET_CHART: 'chart/SET_CHART',
  SET_SELECTED_NODE: 'chart/SET_SELECTED_NODE',
  SET_NODE_DATA: 'chart/SET_NODE_DATA',
  SET_FLOW_DATA: 'chart/SET_FLOW_DATA',
  DELETE_NODE_DATA: 'chart/DELETE_NODE_DATA',
  RESET_CHART: 'chart/RESET_CHART',
};


// Reducer
const initialState = {
  chart: CHART_INITAL_STATE,
  selectedNode: null,
  nodesData: [],
};

const chart = (state = initialState, action) => {
  switch (action.type) {
    case Types.SET_NODE_DATA: {
      const nodeIndex = state.nodesData
        .findIndex((item) => item.id === action.payload.nodeId);
      if (nodeIndex > -1) {
        const newState = state.nodesData;
        if (action.payload.withArray) {
          newState[nodeIndex] = {
            id: action.payload.nodeId,
            data: {
              conditions: state.nodesData[nodeIndex].data.conditions
                .map((item) => ({
                  ...item,
                  [action.payload.fieldName]: action.payload.fieldData,
                })),
            },
          };
        } else {
          newState[nodeIndex] = {
            id: action.payload.nodeId,
            data: {
              ...state.nodesData[nodeIndex].data,
              [action.payload.fieldName]: action.payload.fieldData,
            },
          };
        }
        return {
          ...state,
          nodesData: [
            ...newState,
          ],
        };
      }

      if (action.payload.withArray) {
        return {
          ...state,
          nodesData: [
            ...state.nodesData,
            {
              id: action.payload.nodeId,
              data: {
                conditions: [
                  {
                    [action.payload.fieldName]: action.payload.fieldData,
                  },
                ],
              },
            },
          ],
        };
      }
      return {
        ...state,
        nodesData: [
          ...state.nodesData,
          {
            id: action.payload.nodeId,
            data: {
              [action.payload.fieldName]: action.payload.fieldData,
            },
          },
        ],
      };
    }
    case Types.SET_SELECTED_NODE:
      return {
        ...state,
        selectedNode: action.payload.nodeId,
      };
    case Types.SET_CHART_BY_ACTIONS:
      return {
        ...state,
        chart: {
          ...action.payload.oldChart,
          ...action.payload.newChart,
        },
      };
    case Types.SET_CHART:
      return {
        ...state,
        chart: action.payload.chartData,
      };
    case Types.SET_FLOW_DATA:
      return {
        ...state,
        nodesData: action.payload.flowData,
      };
    case Types.DELETE_NODE_DATA:
      return {
        ...state,
        nodesData: state.nodesData
          .filter((item) => item.id !== action.payload.id),
      };
    case Types.RESET_CHART:
      return {
        ...state,
        chart: {
          offset: {
            x: 0,
            y: 0,
          },
          nodes: {
            start: {
              id: 'start',
              type: 'start',
              position: {
                x: 100,
                y: 300,
              },
              properties: {
                name: 'start',
              },
              ports: {
                output1: {
                  id: 'output1',
                  type: 'right',
                },
              },
            },
          },
          links: {},
          selected: {},
          hovered: {},
        },
      };
    default:
      return state;
  }
};

export default chart;


// Action Creators
export const Creators = {
  setNodesData: (nodeId, fieldName, fieldData, withArray = false) => ({
    type: Types.SET_NODE_DATA,
    payload: {
      nodeId, fieldName, fieldData, withArray,
    },
  }),
  setSelectedNode: (nodeId) => ({
    type: Types.SET_SELECTED_NODE,
    payload: { nodeId },
  }),
  setChartByActions: (oldChart, newChart) => ({
    type: Types.SET_CHART_BY_ACTIONS,
    payload: { oldChart, newChart },
  }),
  setChart: (chartData) => ({
    type: Types.SET_CHART,
    payload: { chartData },
  }),
  setFlowData: (flowData) => ({
    type: Types.SET_FLOW_DATA,
    payload: { flowData },
  }),
  deleteNodeData: (id) => ({
    type: Types.DELETE_NODE_DATA,
    payload: { id },
  }),
  resetChart: () => ({
    type: Types.RESET_CHART,
  }),
};

import React from 'react';
import ReactModal from 'react-modal';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import DefaultLayout from './components/DefaultLayout';
import GlobalStyle from './styles/global';


export default function App() {
  toast.configure({
    autoClose: 3000,
    draggable: false,
  });
  ReactModal.setAppElement('#root');
  return (
    <>
      <ToastContainer />
      <GlobalStyle />
      <BrowserRouter>
        <DefaultLayout />
      </BrowserRouter>
    </>
  );
}

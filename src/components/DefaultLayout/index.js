import React from 'react';
import { useLocation } from 'react-router-dom';

import 'react-toastify/dist/ReactToastify.css';
import Routes from '../../routes';
import Header from '../Header';

export default function DefaultLayout() {
  const location = useLocation();

  const renderHeader = () => {
    const { pathname } = location;
    if (!pathname.includes('login')
      && !pathname.includes('studio')
      && !pathname.includes('signup')
      && !pathname.includes('forgot-my-password')
      && !pathname.includes('reset-my-password')) {
      return <Header />;
    }
    return null;
  };

  return (
    <>
      {
        renderHeader()
      }
      <Routes />
    </>
  );
}

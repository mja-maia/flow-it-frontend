import React from 'react';

import { Container } from './styles';

export default function TableCol({ children }) {
  return (
    <Container>{children}</Container>
  );
}

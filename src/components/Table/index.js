/* eslint-disable react/no-array-index-key */
import React from 'react';

import {
  Container, Header, HeaderItem, Body,
} from './styles';

export default function Table({ headers, children }) {
  return (
    <Container>
      <Header>
        {headers.map((headerItem, index) => (
          <HeaderItem key={index}>{headerItem}</HeaderItem>
        ))}
      </Header>
      <Body>
        {children}
      </Body>
    </Container>
  );
}

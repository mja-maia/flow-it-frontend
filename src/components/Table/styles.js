import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 16px;
`;

export const Header = styled.div`
  background-color: #d6d6d6;
  color: black;
  height: 40px;

  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export const HeaderItem = styled.div`
  height: 100%;
  width: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Row = styled.div`
  cursor: pointer;
  height: 60px;

  display: flex;
  justify-content: space-around;
  align-items: center;

  &:nth-of-type(odd){
    background-color: white;
  }

  &:hover{
    background-color: #e6e6e6;
  }
`;

export const Body = styled.div`
  border: 1px solid rgba(36,111,197,0.2);
  border-top: none;
`;


export const Col = styled.div`
  width: 100%;

  display: flex;
  justify-content: center;
`;

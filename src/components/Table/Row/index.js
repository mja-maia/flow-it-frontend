import React from 'react';

import { Container } from './styles';

export default function TableRow({
  children, onClick, hovered, style,
}) {
  return (
    <Container
      style={style}
      hovered={hovered}
      onClick={onClick}
    >{children}
    </Container>
  );
}

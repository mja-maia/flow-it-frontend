import styled from 'styled-components';

export const Container = styled.div`
  cursor: pointer;
  height: 60px;

  display: flex;
  justify-content: space-around;
  align-items: center;

  &:nth-of-type(odd){
    background-color: white;
  }

  &:hover{
    background-color: ${(props) => (props.hovered ? '#e6e6e6' : '')};
  }
`;

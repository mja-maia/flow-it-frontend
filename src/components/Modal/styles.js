import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  /* justify-content: space-between; */
  height: 100%;
`;

export const Header = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const CloseButton = styled.div`
  cursor: pointer;
`;

export const Content = styled.div`
  flex: 1;
`;

export const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
  position: relative;
  margin-top: 16px;
`;

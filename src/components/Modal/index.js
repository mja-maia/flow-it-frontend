import React from 'react';
import {
  MdClose,
} from 'react-icons/md';
import * as ReactModal from 'react-modal';

import { Button } from '@material-ui/core';

import {
  Container, Header, Content, CloseButton, Footer,
} from './styles';

export default function Modal({
  children,
  isOpen,
  width,
  height,
  setModalIsOpen,
  footerButton,
  onClick,
  buttonText,
  disabled,
}) {
  return (
    <ReactModal
      isOpen={isOpen}
      style={{
        overlay: {},
        content: {
          position: 'absolute',
          top: '20%',
          left: '50%',
          marginLeft: `-${width / 2}px`,
          width,
          height,
        },
      }}
    >
      <Container>
        <Header>
          <CloseButton onClick={() => setModalIsOpen(false)}>
            <MdClose />
          </CloseButton>
        </Header>
        <Content>
          {children}
        </Content>
        {
          footerButton && (
            <Footer width={width}>
              <Button
                disabled={disabled}
                onClick={onClick}
                variant="contained"
                color="primary"
              >{buttonText}
              </Button>
            </Footer>
          )
        }
      </Container>
    </ReactModal>
  );
}

import React from 'react';

import { Container as ContainerStyled } from './styles';

export default function Container({ children }) {
  return (
    <ContainerStyled>
      {children}
    </ContainerStyled>
  );
}

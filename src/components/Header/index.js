import React from 'react';
import { useHistory } from 'react-router-dom';

import pathLogo from '../../assets/images/logo.svg';
import { logout } from '../../services/auth';
import {
  Container, Logo, Menu, MenuItem, LogoutButton,
} from './styles';


export default function Header() {
  const history = useHistory();
  const handleClick = (route) => {
    history.push(route);
  };

  return (
    <Container>
      <Logo>
        <img src={pathLogo} alt="Logo" />
        <div>FlowYt</div>
      </Logo>
      <Menu>
        <MenuItem>Monitoring</MenuItem>
        <MenuItem onClick={() => handleClick('/workspaces')}>Builder</MenuItem>
        <MenuItem onClick={() => handleClick('/teams')}>Teams</MenuItem>
      </Menu>
      <LogoutButton onClick={() => {
        history.push('/login');
        logout();
      }}
      >
        Logout
      </LogoutButton>
    </Container>
  );
}

import styled from 'styled-components';

export const Container = styled.div`
  color: white;
  height: 5%;
  background-color: #7159C1;
  font-weight: 600;

  display: flex;
  align-items: center;
`;


export const Logo = styled.div`
  width: 200px;
  padding-left: 16px;
  display: flex;
  align-items: center;

  div{
    margin-left: 8px;
  }

  img{
    width: 34px;
  }
`;

export const Menu = styled.div`
  height: 100%;

  display: flex;
  justify-content: center;
  width: 100%;
  align-items: center;
`;

export const MenuItem = styled.div`
  margin: 0 16px;
  cursor: pointer;
`;

export const LogoutButton = styled.div`
  cursor: pointer;
  margin-right: 10px;
`;

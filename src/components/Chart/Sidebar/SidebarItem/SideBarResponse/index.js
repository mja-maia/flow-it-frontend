import React, { useEffect, useState } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useSelector } from 'react-redux';

import FormGroup from '../../../../Form/FormGroup';
import Input from '../../../../Form/Input';
import Label from '../../../../Form/Label';
import { Container, CodeWrapper } from './styles';

export default function SideBarResponse({ handleSideBarChange }) {
  const chart = useSelector((state) => state.chart);

  const [formData, setFormData] = useState({
    status: '',
    headers: '',
    data: '',
  });


  useEffect(() => {
    const nodeData = chart.nodesData
      .find((node) => node.id === chart.selectedNode);
    if (nodeData) {
      setFormData(nodeData.data);
    } else {
      setFormData({
        status: '',
        headers: '',
        data: '',
      });
    }
  }, [chart.nodesData, chart.selectedNode]);

  return (
    <Container>
      <FormGroup>
        <Label>Status:</Label>
        <Input
          name="status"
          value={formData.status}
          onChange={handleSideBarChange}
        />
      </FormGroup>
      <CodeWrapper style={{ marginTop: 16 }}>
        <Label>Headers:</Label>
        <MonacoEditor
          language="json"
          theme="vs-dark"
          height="200"
          value={formData.headers}
          onChange={(val) => {
            setFormData({
              ...formData,
              headers: val,
            });
            handleSideBarChange(null, val, 'headers');
          }}
        />
      </CodeWrapper>
      <CodeWrapper style={{ marginTop: 16 }}>
        <Label>Data:</Label>
        <MonacoEditor
          language="json"
          theme="vs-dark"
          height="200"
          value={formData.data}
          onChange={(val) => {
            setFormData({
              ...formData,
              data: val,
            });
            handleSideBarChange(null, val, 'data');
          }}
        />
      </CodeWrapper>
    </Container>
  );
}

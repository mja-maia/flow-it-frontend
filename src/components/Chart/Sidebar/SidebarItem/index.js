import React from 'react';

import { Container, Outer } from './styles';

export default function SidebarItem({ type, ports, properties }) {
  return (
    <Container>
      <Outer
        draggable
        onDragStart={(event) => {
          event.dataTransfer
            .setData('react-flow-chart',
              JSON.stringify({ type, ports, properties }));
        }}
      >
        {type}
      </Outer>
    </Container>
  );
}

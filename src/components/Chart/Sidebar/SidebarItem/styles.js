import styled from 'styled-components';

export const Container = styled.div`
  border-bottom: 1px solid #ccc;
`;

export const Outer = styled.div`
  padding: 20px 30px;
  font-size: 14px;
  background: white;
  cursor: move;
`;

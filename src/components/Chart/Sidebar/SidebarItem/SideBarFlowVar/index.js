import React, { useEffect, useState } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useSelector } from 'react-redux';

import Label from '../../../../Form/Label';
import { Container, CodeWrapper } from './styles';

export default function SideBarFlowVar({ handleSideBarChange }) {
  const chart = useSelector((state) => state.chart);

  const [data, setData] = useState('');

  useEffect(() => {
    const nodeData = chart.nodesData
      .find((node) => node.id === chart.selectedNode);
    if (nodeData) {
      setData(nodeData.data);
    } else {
      setData('');
    }
  }, [chart.nodesData, chart.selectedNode]);

  return (
    <Container>
      <CodeWrapper style={{ marginTop: 16 }}>
        <Label>Data:</Label>
        <MonacoEditor
          language="json"
          theme="vs-dark"
          height="200"
          value={data.data}
          onChange={(val) => {
            setData(val);
            handleSideBarChange(null, val, 'data');
          }}
        />
      </CodeWrapper>
    </Container>
  );
}

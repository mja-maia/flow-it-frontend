import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { useDebouncedCallback } from 'use-debounce';

import api from '../../../../../services/api';
import { Creators as ChartActions } from '../../../../../store/ducks/chart';
import FormGroup from '../../../../Form/FormGroup';
import Input from '../../../../Form/Input';
import Label from '../../../../Form/Label';
import Select from '../../../../Form/Select';
import { Container } from './styles';

export default function SideBarIf() {
  const chart = useSelector((state) => state.chart);
  const dispatch = useDispatch();
  const { flowId } = useParams();
  const [formData, setFormData] = useState({
    conditions: [
      {
        first_expression: '',
        operator: 'equal',
        second_expression: '',
      },
    ],
  });

  const [saveFlowDataDebouncedCallback] = useDebouncedCallback(
    async () => {
      // debugger;
      const result = await api.patch(`/flows/${flowId}/`, {
        flow_data: chart.nodesData,
      });
      if (result.status === 200) {
        // mostra aviso de salvamento
      }
    },
    1500,
  );

  const handleChange = (event) => {
    dispatch(ChartActions
      .setNodesData(chart.selectedNode,
        event.target.name, event.target.value, true));
    saveFlowDataDebouncedCallback();
  };

  useEffect(() => {
    const nodeData = chart.nodesData
      .find((node) => node.id === chart.selectedNode);
    if (nodeData) {
      console.log('nodeData', nodeData);
      setFormData(nodeData.data);
    } else {
      setFormData({
        conditions: [
          {
            first_expression: '',
            operator: 'equal',
            second_expression: '',
          },
        ],
      });
    }
  }, [chart.nodesData, chart.selectedNode]);

  return (
    <Container>
      <FormGroup>
        <Label>Valor 1:</Label>
        <Input
          name="first_expression"
          value={formData.conditions[0].first_expression}
          onChange={(event) => {
            setFormData({
              ...formData,
              conditions: formData.conditions.map((item) => ({
                ...item,
                first_expression: event.target.value,
              })),
            });
            handleChange(event);
          }}
        />
      </FormGroup>
      <FormGroup>
        <Label>Operador:</Label>
        <Select
          name="operator"
          value={formData.conditions[0].operator}
          onChange={(event) => {
            setFormData({
              ...formData,
              conditions: formData.conditions.map((item) => ({
                ...item,
                operator: event.target.value,
              })),
            });
            handleChange(event);
          }}
        >
          <option value="equal">Igual</option>
          <option value="different">Diferente</option>
          <option value="greater_than">Maior que</option>
          <option value="less_than">Menor que</option>
        </Select>
      </FormGroup>
      <FormGroup>
        <Label>Valor 2:</Label>
        <Input
          name="second_expression"
          value={formData.conditions[0].second_expression}
          onChange={(event) => {
            setFormData({
              ...formData,
              conditions: formData.conditions.map((item) => ({
                ...item,
                second_expression: event.target.value,
              })),
            });
            handleChange(event);
          }}
        />
      </FormGroup>
    </Container>
  );
}

import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { useDebouncedCallback } from 'use-debounce';

import api from '../../../../../services/api';
import { Creators as ChartActions } from '../../../../../store/ducks/chart';
import FormGroup from '../../../../Form/FormGroup';
import { Container } from './styles';

export default function SideBarJump() {
  const { id, flowId } = useParams();
  const dispatch = useDispatch();
  const chart = useSelector((state) => state.chart);
  const [flowOptions, setFlowOptions] = useState([]);
  const [selectedFlow, setSelectedFlow] = useState(null);

  const [saveFlowDataDebouncedCallback] = useDebouncedCallback(
    () => {
      api.patch(`/flows/${flowId}/`, {
        flow_data: chart.nodesData,
      });
    },
    1500,
  );

  const handleChange = (e, newValue) => {
    dispatch(ChartActions
      .setNodesData(chart.selectedNode, 'next_flow', newValue.name));
    saveFlowDataDebouncedCallback();
  };


  useEffect(() => {
    const fetchFlows = async () => {
      try {
        const result = await api.get(`/flows/?workspace__id=${id}`);
        if (result.status === 200) {
          const { data } = result;
          const flows = data.results.filter((item) => item.id !== flowId);
          setFlowOptions(flows);

          const nodeData = chart.nodesData
            .find((node) => node.id === chart.selectedNode);
          if (nodeData) {
            setSelectedFlow(
              flows.find((item) => item.name === nodeData.data.next_flow),
            );
          }
        }
      } catch (e) {
        toast.error('Something wrong happened');
      }
    };

    fetchFlows();
  }, [id, chart.nodesData, chart.selectedNode, flowId]);

  return (
    <Container>
      <FormGroup>
        <Autocomplete
          id="flows-combo"
          options={flowOptions}
          value={selectedFlow}
          onChange={(e, newValue) => handleChange(e, newValue)}
          getOptionLabel={(option) => option.name}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Flow"
              variant="outlined"
            />
          )}
        />
      </FormGroup>
    </Container>
  );
}

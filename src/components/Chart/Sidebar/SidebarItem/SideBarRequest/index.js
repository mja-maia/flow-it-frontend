import React, { useEffect, useState } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import FormGroup from '../../../../Form/FormGroup';
import Input from '../../../../Form/Input';
import Label from '../../../../Form/Label';
import Select from '../../../../Form/Select';
import { Container, CodeWrapper } from './styles';


export default function SideBarRequest({ handleSideBarChange }) {
  const chart = useSelector((state) => state.chart);
  const { flowId } = useParams();

  const [formData, setFormData] = useState({
    headers: '',
    data: '',
    url: '',
    method: 'GET',
  });

  useEffect(() => {
    const nodeData = chart.nodesData
      .find((node) => node.id === chart.selectedNode);
    if (nodeData) {
      setFormData(nodeData.data);
    } else {
      setFormData({
        headers: '',
        data: '',
        url: '',
        method: 'GET',
      });
    }
  }, [chart.nodesData, chart.selectedNode]);

  return (
    <Container>
      <FormGroup>
        <Label>URL:</Label>
        <Input
          value={formData.url}
          name="url"
          onChange={(e) => {
            setFormData({
              ...formData,
              url: e.target.value,
            });
            handleSideBarChange(null, e.target.value, 'url', flowId);
          }}
        />
      </FormGroup>
      <FormGroup>
        <Label>Method:</Label>
        <Select
          name="method"
          value={formData.method}
          onChange={(e) => {
            setFormData({
              ...formData,
              method: e.target.value,
            });
            handleSideBarChange(null, e.target.value, 'method', flowId);
          }}
        >
          <option value="GET">GET</option>
          <option value="POST">POST</option>
          <option value="PUT">PUT</option>
          <option value="DELETE">DELETE</option>
          <option value="PATCH">PATCH</option>
          <option value="TRACE">TRACE</option>
          <option value="OPTIONS">OPTIONS</option>
          <option value="CONNECT">CONNECT</option>
        </Select>
      </FormGroup>
      <CodeWrapper style={{ marginTop: 16 }}>
        <Label>Headers:</Label>
        <MonacoEditor
          language="json"
          theme="vs-dark"
          height="200"
          value={formData.headers}
          onChange={(val) => {
            setFormData({
              ...formData,
              headers: val,
            });
            handleSideBarChange(null, val, 'headers', flowId);
          }}
        />
      </CodeWrapper>
      {
        formData.method === 'POST' && (
          <CodeWrapper style={{ marginTop: 16 }}>
            <Label>Body:</Label>
            <MonacoEditor
              language="json"
              theme="vs-dark"
              height="200"
              value={formData.data}
              onChange={(val) => {
                setFormData({
                  ...formData,
                  data: val,
                });
                handleSideBarChange(null, val, 'data', flowId);
              }}
            />
          </CodeWrapper>
        )
      }
    </Container>
  );
}

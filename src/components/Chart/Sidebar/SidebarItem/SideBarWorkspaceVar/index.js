import React, { useEffect, useState } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useSelector } from 'react-redux';

import Label from '../../../../Form/Label';
import { Container, CodeWrapper } from './styles';

export default function SideBarWorkspaceVar({ handleSideBarChange }) {
  const chart = useSelector((state) => state.chart);

  const [formData, setFormData] = useState({
    data: '',
  });

  useEffect(() => {
    const nodeData = chart.nodesData
      .find((node) => node.id === chart.selectedNode);
    if (nodeData) {
      setFormData(nodeData.data);
    } else {
      setFormData({
        data: '',
      });
    }
  }, [chart.nodesData, chart.selectedNode]);

  return (
    <Container>
      <CodeWrapper style={{ marginTop: 16 }}>
        <Label>Data:</Label>
        <MonacoEditor
          language="json"
          theme="vs-dark"
          height="200"
          value={formData.data}
          onChange={(val) => {
            setFormData({
              ...formData,
              data: val,
            });
            handleSideBarChange(null, val, 'data');
          }}
        />
      </CodeWrapper>
    </Container>
  );
}

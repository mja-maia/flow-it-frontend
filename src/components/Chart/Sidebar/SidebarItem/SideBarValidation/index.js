import React, { useEffect, useState } from 'react';
import MonacoEditor from 'react-monaco-editor';
import { useSelector } from 'react-redux';

import Label from '../../../../Form/Label';
import { Container, CodeWrapper } from './styles';

export default function SideBarValidation({ handleSideBarChange }) {
  const chart = useSelector((state) => state.chart);

  const [formData, setFormData] = useState({
    schema: '',
  });

  useEffect(() => {
    const nodeData = chart.nodesData
      .find((node) => node.id === chart.selectedNode);
    if (nodeData) {
      setFormData(nodeData.data);
    }
  }, [chart.nodesData, chart.selectedNode]);

  return (
    <Container>
      <CodeWrapper style={{ marginTop: 16 }}>
        <Label>Schema:</Label>
        <MonacoEditor
          language="json"
          theme="vs-dark"
          height="200"
          value={formData.schema}
          onChange={(val) => {
            setFormData({
              ...formData,
              schema: val,
            });
            handleSideBarChange(null, val, 'schema');
          }}
        />
      </CodeWrapper>
    </Container>
  );
}

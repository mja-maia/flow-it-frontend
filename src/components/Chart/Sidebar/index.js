import React from 'react';
import { useHistory, useParams } from 'react-router-dom';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import SidebarItem from './SidebarItem';
import { Container, BackButton } from './styles';


export default function Sidebar() {
  const history = useHistory();
  const { id } = useParams();
  return (
    <Container>
      <BackButton>
        <span
          role="button"
          tabIndex="0"
          onKeyPress={() => {}}
          onClick={() => history.push(`/workspace/${id}/flows`)}
        >
          <ArrowBackIcon />
          Go Back
        </span>
      </BackButton>
      <SidebarItem
        type="Flow Var"
        ports={{
          input1: {
            id: 'input1',
            type: 'left',
          },
          output1: {
            id: 'output1',
            type: 'right',
          },
        }}
        properties={{
          name: 'flow_var',
        }}
      />
      <SidebarItem
        type="Request"
        ports={{
          input: {
            id: 'input',
            type: 'left',
            properties: {
              value: 'input',
            },
          },
          output1: {
            id: 'output1',
            type: 'right',
            properties: {
              value: 'yes',
            },
          },
          output2: {
            id: 'output2',
            type: 'right',
            properties: {
              value: 'no',
            },
          },
        }}
        properties={{
          name: 'request',
        }}
      />
      <SidebarItem
        type="Workspace Var"
        ports={{
          input: {
            id: 'input',
            type: 'left',
            properties: {
              value: 'input',
            },
          },
          output1: {
            id: 'output1',
            type: 'right',
          },
        }}
        properties={{
          name: 'workspace_var',
        }}
      />
      <SidebarItem
        type="Response"
        ports={{
          input1: {
            id: 'input1',
            type: 'left',
            properties: {
              value: 'input',
            },
          },
        }}
        properties={{
          name: 'response',
        }}
      />
      <SidebarItem
        type="Conditional"
        ports={{
          input1: {
            id: 'input1',
            type: 'left',
            properties: {
              value: 'input',
            },
          },
          output1: {
            id: 'output1',
            type: 'right',
            properties: {
              value: 'yes',
            },
          },
          output2: {
            id: 'output2',
            type: 'right',
            properties: {
              value: 'no',
            },
          },
        }}
        properties={{
          name: 'if',
        }}
      />
      <SidebarItem
        type="Jump"
        ports={{
          input1: {
            id: 'input1',
            type: 'left',
            properties: {
              value: 'input',
            },
          },
        }}
        properties={{
          name: 'jump',
        }}
      />
      <SidebarItem
        type="Validation"
        ports={{
          input1: {
            id: 'input1',
            type: 'left',
          },
          output1: {
            id: 'output1',
            type: 'right',
            properties: {
              value: 'yes',
            },
          },
          output2: {
            id: 'output2',
            type: 'right',
            properties: {
              value: 'no',
            },
          },
        }}
        properties={{
          name: 'validation',
        }}
      />
    </Container>
  );
}

import styled from 'styled-components';

export const Container = styled.div`
  width: 15%;
  background: white;
  border-right: 1px solid #ccc;
`;

export const BackButton = styled.div`
  border-bottom: 1px solid #ccc;
  padding: 20px 30px;
  font-size: 14px;

  span{
    outline: none;
    display: flex;
    align-items: center;
    cursor: pointer;
  }

  svg{
    margin-right: 4px;
    cursor: pointer;
    width: 20px;
    height: 20px;
  }
`;

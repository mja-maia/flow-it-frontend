import React from 'react';
import {
  MdClose, MdCheck,
} from 'react-icons/md';

import { Container } from './styles';

export default function Port({ port }) {
  return (
    <Container>
      { port.properties && port.properties.value === 'yes' && (
        <MdCheck style={{ color: 'white' }} />
      )}
      { port.properties && port.properties.value === 'no' && (
        <MdClose style={{ color: 'white' }} />

      )}
    </Container>
  );
}

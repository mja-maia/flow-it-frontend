import styled from 'styled-components';

export const Container = styled.div`
  font-size: 12px;
  color: white;
  position: absolute;
  right: 20px;
  top: 20px;
  background-color: #7159C1;
  border-radius: 6px;
  padding: 16px;
  z-index: 999;
  visibility: ${(props) => (props.showSaveMark ? 'visible' : 'hidden')};
  animation: ${(props) => (props.showSaveMark ? 'fadeIn' : 'fadeOut')}
  0.5s linear;
  transition: visibility 0.5 linear;

  display: flex;
  align-items: center;
  justify-content: center;

  span{
    margin-left: 8px;
  }
`;

import React from 'react';

import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

import { Container } from './styles';

export default function SavedMark({ showSaveMark }) {
  return (
    <Container showSaveMark={showSaveMark}>
      <CheckCircleOutlineIcon />
      <span>Your flow has been saved!</span>
    </Container>
  );
}

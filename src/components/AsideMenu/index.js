import React from 'react';

import { Container } from './styles';

export default function AsideMenu({ children }) {
  return (
    <Container>
      {children}
    </Container>
  );
}

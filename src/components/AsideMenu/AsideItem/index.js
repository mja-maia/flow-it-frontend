import React from 'react';

import { Container } from './styles';

export default function AsideItem({ children, onClick }) {
  return (
    <Container onClick={onClick}>
      {children}
    </Container>
  );
}

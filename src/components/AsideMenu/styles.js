import styled from 'styled-components';

export const Container = styled.div`
  width: 15%;
  height: 100%;
  background: white;
  color: #9b9b9b;
`;

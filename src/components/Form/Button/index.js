import React from 'react';

import { Container } from './styles';

export default function Button({
  children, width, style, onClick, disabled, type,
}) {
  return (
    <Container
      width={width}
      style={style}
      type={type}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </Container>
  );
}

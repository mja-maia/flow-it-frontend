import styled from 'styled-components';

export const Container = styled.button`
  outline: none;
  border: 1px solid #ebebeb;
  padding: 10px;
  background: ${(props) => (props.type === 'secondary'
    ? '#7159C1' : 'transparent')};
  color: ${(props) => (props.type === 'secondary'
    ? 'white' : '#545454')};
  font-size: 16px;
  font-weight: 500;
  border-radius: 8px;
  width: ${(props) => (props.width ? props.width : '150')}px;
`;

import styled from 'styled-components';

export const Container = styled.div`
  color: #242424;
  margin-bottom: 8px;
  font-weight: 600;
`;

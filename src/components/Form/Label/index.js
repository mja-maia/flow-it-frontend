import React from 'react';

import { Container } from './styles';

export default function Label({ children }) {
  return (
    <Container>
      {children}
    </Container>
  );
}

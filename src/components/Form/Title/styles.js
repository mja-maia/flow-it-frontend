import styled from 'styled-components';

export const Container = styled.h1`
  line-height: 30px;
  font-size: 20px;
  font-weight: 300;
  color: rgb(115,115,115);
`;

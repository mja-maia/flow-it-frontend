import { FormControl } from '@material-ui/core';
import styled from 'styled-components';


export const Container = styled(FormControl)`
  width: 100%;
`;

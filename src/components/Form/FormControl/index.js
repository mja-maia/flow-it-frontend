/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import { Container } from './styles';

function FormControl({ children, ...rest }) {
  return (
    <Container {...rest}>
      {children}
    </Container>
  );
}

export default FormControl;

import React from 'react';

import { Container } from './styles';

export default function Input({ onChange, value, name }) {
  return (
    <Container>
      <input name={name} type="text" value={value} onChange={onChange} />
    </Container>
  );
}

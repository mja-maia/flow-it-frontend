import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100%;
  input{
    border: 1px solid #ccc;
    outline:none;
    font-size: 16px;
    width: 100%;
    height: 100%;
  }
`;

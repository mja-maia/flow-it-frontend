import React from 'react';

import { Container } from './styles';

export default function Select({
  children, onChange, value, name,
}) {
  return (
    <Container>
      <select name={name} onChange={onChange} value={value}>
        {children}
      </select>
    </Container>
  );
}
